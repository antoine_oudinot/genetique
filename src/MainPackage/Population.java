package MainPackage; /***********************************************************************
 * Module:  Population.java
 * Author:  NathanFolb
 * Purpose: Defines the Class Population
 ***********************************************************************/

import java.util.*;

public class Population {
   public java.util.Collection<Individu> individu;

   public Population evoluer(Environnement cible) {
      // TODO: implement
      return null;
   }

   public int size() {
      // TODO: implement
      return 0;
   }

   public Population() {
   }

   public void addIndividu(Individu newIndividu) {
      if (newIndividu == null)
         return;
      if (this.individu == null)
         this.individu = new java.util.HashSet<Individu>();
      if (!this.individu.contains(newIndividu))
         this.individu.add(newIndividu);
   }

}