package MainPackage; /***********************************************************************
 * Module:  PopulationArray.java
 * Author:  NathanFolb
 * Purpose: Defines the Class PopulationArray
 ***********************************************************************/

import java.util.*;

public class PopulationArray {
   private static final int POP_SIZE = 20;
   private int size = 0;
   private Individu[] individus;

   public PopulationArray() {
      individus = new Individu[POP_SIZE];
   }

   public int size() {
      return size;
   }

   public void add(Individu individu) {
      if(size < individus.length){
         individus[size] = individu;
         size++;
      }else{
         throw new ArrayIndexOutOfBoundsException("Plus de place !");
      }
   }

   public String toString() {
      return Arrays.toString(individus);
   }

}