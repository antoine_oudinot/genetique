package main; /***********************************************************************
 * Module:  Environnement.java
 * Author:  AshenOne
 * Purpose: Defines the Class Environnement
 ***********************************************************************/

import java.util.*;


public interface Environnement {
    public double eval(Individu i);
}