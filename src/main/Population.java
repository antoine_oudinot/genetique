package main; /***********************************************************************
 * Module:  Population.java
 * Author:  AshenOne
 * Purpose: Defines the Class Population
 ***********************************************************************/

import java.util.*;

public class Population{

    private ArrayList<Individu> individu;

    public Population evoluer(Environnement cible) {
        // TODO: implement
        return null;
    }

    public int size() {
        // TODO: implement
        return 0;
    }

    public Population() {

    }

    public void addIndividu(Individu newIndividu) {
        if (newIndividu == null)
            return;
        if (this.individu == null)
            this.individu = new ArrayList<>();
        if (!this.individu.contains(newIndividu))
            this.individu.add(newIndividu);
    }

    public void evaluer(Environnement cible){
      	individu.forEach(i-> cible.eval(i));
      	//tri par fitness décroissante
      	Collections.sort(individu,new Individu());
    }

    private void muter(double prob){

    }

    public void displayIndividu() {
        individu.forEach(System.out::println);
    }
    
    public ArrayList<Individu> getIndividus(){
    	return this.individu;
    }
    
}