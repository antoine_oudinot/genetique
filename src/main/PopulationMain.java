package main;

import java.util.ArrayList;
import java.util.Scanner;

public class PopulationMain {

    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);

        System.out.println("taille de la population ?");
        int size = scan.nextInt();

        Population pop = PopulationFactory.createRandomPopulation(size);

        pop.displayIndividu();
        ValeurCible cible = new ValeurCible();
        pop.evaluer(cible);
        
        System.out.println("Population �valu�e : ");
        pop.displayIndividu();
        
    }
}