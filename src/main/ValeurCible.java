package main;

import java.util.Random;

public class ValeurCible implements Environnement {
    @Override
    public double eval(Individu i) {
		Random random = new Random();
		double valueEnvir = random.nextDouble()*10;
    	double newFitness = 1/((valueEnvir-i.getValeur())*(valueEnvir-i.getValeur()));
    	i.setFitness(newFitness);
        return newFitness;
    }
}
