package main; /***********************************************************************
 * Module:  Individu.java
 * Author:  AshenOne
 * Purpose: Defines the Class Individu
 ***********************************************************************/

import java.util.*;

public class Individu implements Comparator<Individu>{
    private Double valeur;
    private Double fitness = 0.0;
    private Random random;

    public Individu() {
        random = new Random();
        valeur = random.nextDouble()*10;
    }
    public Individu(Double valeur) {
        this.valeur = valeur;
        random = new Random();
    }

    public double getValeur() {
    	return this.valeur;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    public void muter(){
        double valeur = this.valeur;

        double pourcentVal = valeur * 0.1;
        Random random = new Random();

        this.valeur =  (Math.random() * ((valeur + pourcentVal) -  (valeur - pourcentVal))) + (valeur - pourcentVal);
    }

    public Individu croiser(Individu autre){
        return new Individu( (this.valeur + autre.valeur) / 2);
    }

    @Override
    public String toString() {
        return "Individu{" +
                "valeur=" + valeur +
                ", fitness=" + fitness +
                '}';
    }
	@Override
	public int compare(Individu o1, Individu o2) {
		// TODO Auto-generated method stub
		return o1.fitness.compareTo(o2.fitness);
	}
}