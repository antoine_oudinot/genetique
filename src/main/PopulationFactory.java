package main;

import java.util.Random;

/***********************************************************************
 * Module:  PopulationFactory.java
 * Author:  AshenOne
 * Purpose: Defines the Class PopulationFactory
 ***********************************************************************/


public class PopulationFactory {

   public static void main(String[] args){

   }

   public PopulationFactory() {
   }

   public static Population createRandomPopulation(int nb) {
        Population pop = new Population();

       Random rnd = new Random();

       for (int i = 0; i < nb; i++){
           pop.addIndividu(new Individu());
       }

       return pop;
   }

}